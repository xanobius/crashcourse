<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = new \App\User();
        $user->email = 'test@example.com';
        $user->name = 'Hans';
        $user->password = bcrypt('asd123');
        $user->save();

    }
}
